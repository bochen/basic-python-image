Quick Start
===========

   - Fork this repo
   - Wait for ci to build
      + **NOTE:** this might not happen automatically, you can trigger
        manually by clicking on the "CI/CD" tab on the left and then
        pushing "Run Pipeline" on the top
   - Go to lxplus and run

     ```
     singularity run -B /eos:/eos docker://gitlab-registry.cern.ch/${USER}/basic-python-image:latest
     ```

     (the `-B /eos:/eos` just maps your EOS area to the container, you
     can repeat this call for `/cvmfs` or other FUSE file systems)

   - Run whatever scripts you need, you should be able to access
     everything you could normally access on afs.

Adding New Things
=================

You're encouraged to fork this and add whatever you need.

Python Modules
--------------

List these in the `requirements.txt` file

Other programs
--------------

Edit the `Dockerfile` and add a call to `apt-get install -y <package>`


Thanks
======

Thanks to Matthew Feickert for his [example images][1], most things
are copied from there.

[1]: https://gitlab.cern.ch/aml/containers/docker
