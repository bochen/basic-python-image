# bashrc file for the images

# set up command line
PS1='\[\033[01;31m\][\h:\u]\[\033[01;32m\]:\W>\[\033[00m\] '
CLICOLOR=1

# add h5ls tab complete
source /etc/_h5ls.sh

# enable tab complete
source /etc/profile.d/bash_completion.sh
